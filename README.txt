INTRO

Field Value Loader(FVL) is a simple module for hiding and display them by
calling ajax-request of the value of certain fields.

Initially the module was created to maximize the protection of personal data
in the respective fields, not limited access rights from being indexed by
search engines. But can be used for many other purposes.

FEATURES

* The choice of the necessary fields for each bundle of entity.
* Replaces data field on link. With the loading of the corresponding value
of the field after the click.

USAGE

After installing the module you can configure settings by navigating
to: admin/config/content/fvl. Configure which fields should be hidden when
viewing content for manual loading them later (using AJAX). Separately for
each bundle of an entity.

----------------------------------

Credits go to Dalay
(http://www.drupalka.ru/).
